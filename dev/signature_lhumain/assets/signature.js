$(function() {

  /*
   * Variables globales
   */
  var styles = {
    "font-size": "14px",
    "font-family": "Helvetica, Arial, sans-serif",
    "line-height": "120%",
    "color": "black"
  };

  var composantes = [];
  var composantesIndex = [];
  var composanteIdCur = '';
  var adresses = [];
  var adressesIndex = [];
  var adresseIdCur = '';

  /*
   * Fonctions globales
   */
   /*
    * Fonction isUrl(url)
    * Teste si une url est bien formée
    * @param url string url à tester
    * @return boolean
    */
   function isUrl(url) {

     var expression = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
     var regex = new RegExp(expression);
     return url.match(regex);
   }

  /*
   * Fonction sortByKey
   * Trie un tableau json suivant l'une de ses clés
   */
  function sortByKey(array, key) {
    return array.sort(function(a, b) {
      var x = a[key];
      var y = b[key];
      return (x < y ? -1 : 1);
    });
  }

  /*
   * Fonction testPlaceHolder
   * Teste si l'attribut html placeholder est supporté
   */
  function testPlaceHolder() {
    jQuery.support.placeholder = false;
    test = document.createElement('input');
    if ('placeholder' in test) jQuery.support.placeholder = true;

    //Si l'attribut n'est pas supporté, on simule en jquery
    if (!$.support.placeholder) {
      var active = document.activeElement;
      $('input:text,textarea').focus(function() {
        if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
          $(this).val('').removeClass('hasPlaceholder');
        }
      }).blur(function() {
        if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
          $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
        }
      });
      $('input:text,textarea').blur();
      $(active).focus();
    }
  }
  /*
   * Fonction nl2br
   * Transforme les sauts de lignes (newlines) en tags html br (line breaks)
   *
   */
  function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  }

  /*
   * Fonction initialise
   */

  function initialise() {

    //Vérifie le support de l'attribut placeholder
    testPlaceHolder;

    /*
     *  Charge la liste déroulante des composantes
     */
    // Trie les composantes par leur nom court
    composantes = sortByKey(composantes, 'ordre');
    //Crée la liste des composantes dans la liste déroulante
    for (var i = 0; i < composantes.length; i++) {
      var select_text = (composantes[i]['nom_court'] + ' - ' + composantes[i]['nom_long']).substring(0,100);
      var composanteId = composantes[i]['id_composante'];
      $('select#composante').append('<option value="' + composanteId + '">' + select_text + '</option>');
      //On crée un index des composantes
      composantesIndex[composanteId] = i;

      //Si lrsite est renseigné et est connu dans le tableau, on met à jour la composante
      /*
      if ($('#lrsite').val() == composantes[i]['lrsite']) {
        $('select#composante').val(composantes[i]['id']);
      }
      //*/

      $('#pprenom, #pnom, #ptitre, #pservice, #pnumeros1, #pnumeros2, #pcomplement_adresse, #padresse,  #pweb, #pinfos, #pcomposante, #puniversite').css(styles);
      $('#pprenom, #pnom').css({
        "font-weight": "900"
      });

    }

    $('select#composante').val('um3');
    $('input:radio[name="choix_couleur_logo"]').filter('[value="orange"]').prop('checked', true);

    /*
     *  Charge la liste déroulante des adresses postales
     */
    // Trie les adresses par leur nom court
    adresses = sortByKey(adresses, 'id_adresse');

    //Crée la liste des adresses dans la liste déroulante
    for (var i = 0; i < adresses.length; i++) {
      var adresseId = adresses[i]['id_adresse'];
      var select_text = (adresseId + ' - ' + adresses[i]['adresse_postale']).substring(0,70);

      $('select#adresse_postale').append('<option value="' + adresseId + '">' + select_text + '</option>');
      //On crée un index des composantes
      adressesIndex[adresseId] = i;
    }
    if ($('select#adresse_postale').val() == '') $('select#adresse_postale').val('um3-mende');

    //Après une modification de l'email,
    // on supprime les caractères diacritiques qui se sont glissés dans l'email
    $('input#email').change(function() {
      $(this).val(removeDiacritics($(this).val()));
    });

    // Formate le numéro de téléphone
    $('input#tel').val(function(i, value) {
      return value.replace(/(\d{2})(\d{2})(\d{2})(\d{2})(\d{2})/, '$1 $2 $3 $4 $5');
    });

    /*
     * Actions à mener au changement d'adresse postale'
     */
    $('select#adresse_postale').change(function() {
      var adresseId = $(this).val();
      var i = adressesIndex[adresseId];
      $('#padresse').html(adresses[i]['adresse_postale']);
      adresseIdCur = adresseId;
    });

    /*
     * Actions à mener au changement de composante
     */
    $('select#composante').change(function() {
      var composanteId = $(this).val();
      var i = composantesIndex[composanteId];

      // Affiche ou cache le choix de la couleur du logo
      if (composanteId == "um3") {
        $("#choix_couleur_logo").show();
      } else {
        $("#choix_couleur_logo").hide();
      }

      var adresseweb = composantes[i]['adresse_web'];
      if (adresseweb) $('input#web').val(adresseweb);

      // Change la couleur des textes qui doivent changer de couleur
      $('#pprenom, #pnom, #ptellabel, #pfaxlabel, #pmobilelabel, #pcomposante, #puniversite, #pprenom_po, #pnom_po')
      .css('color', composantes[i]['couleur']);

      composanteIdCur = composanteId;

    });

    // Toggle "pour ordre"
    $('#ajouter_po').click(function() {
      $('#po').toggle();
    });

    // Force le changement de composante et d'adresse
    $('select#composante').change();
    $('select#adresse_postale').change();

    /*
     * Validation du formulaire
     */
    $('form#telecharger').submit(function() {
      // Copie la totalité du code html dans le champ 'content'
      // du formulaire pour génération du fichier à télécharger
      $('#content').val($('#signature-externe').html());

      // Remet à zéro la valeur des input
      $(this).find('.hasPlaceholder').each(function() {
        $(this).val('');
      });
      // Envoie le formulaire
      return true;
    });
  }

  /*
   * Fonction previsu
   * Met à jour la prévisualisation de l'image
   * Appelée à intervalle de temps régulier (2s par défaut)
   */
  function previsu() {

    var aidx = adressesIndex[adresseIdCur];
    var cidx = composantesIndex[composanteIdCur];

    //var $plogoetablissement = '<a id="logo-etablissement" href="https://www.univ-montp3.fr/" title="Site web UPV"><img src="' + 'https://signature.univ-montp3.fr/images/logo_um3_' + $('input:radio[name=choix_couleur_logo]:checked').val() + '.png" alt="Logo UM3"  style="display: inline; border: 0px !important;" /></a>';
    //var $plogoetablissement = '<a id="logo-etablissement" href="https://www.univ-montp3.fr/" title="Site web UPV"><img height="71" src="' + 'https://signature.univ-montp3.fr/images/logo_um3_bleu.png" alt="Logo UM3"  style="display: inline; border: 0px !important;" /></a>';
    //var $plogoetablissementnoir = '<a id="logo-etablissement" href="https://www.univ-montp3.fr/" title="Site web UPV"><img height="71" src="' + 'https://signature.univ-montp3.fr/images/logo_um3_noir.png" alt="Logo UM3"  style="display: inline; border: 0px !important;" /></a>';
	var $plogoetablissement = '<a id="logo-etablissement" href="https://www.univ-montp3.fr/" title="Site web UPV"><img height="100" src="' + 'https://lhumain.www.univ-montp3.fr/sites/default/files/lhumain_logo_clr_2.png" alt="Logo UM3"  style="display: inline; border: 0px !important;float:left;" /></a>';
    var $plogoetablissementnoir = '<a id="logo-etablissement" href="https://www.univ-montp3.fr/" title="Site web UPV"><img height="71" src="' + 'https://signature.univ-montp3.fr/images/logo_um3_noir.png" alt="Logo UM3"  style="display: inline; border: 0px !important;float:left" /></a>';


    var $plogocomposante = '<a style="padding-left: 10px;" id="logo-composante" href="" title=""><img height="71" src="' + 'https://signature.univ-montp3.fr/images/logo_' + composanteIdCur + '.png"" alt="Logo composante"  style="display: inline; margin-left: 5px; border: 0px !important;" /></a>';

    // Change le logo
    if (composanteIdCur == "um3") {
      $('#logos').html($plogoetablissement);
    } else {
      $('#logos').html($plogoetablissementnoir + $plogocomposante);

    }

    $('#pprenom').html($('#prenom').val());
    $('#pnom').html($('#nom').val().toUpperCase());

    // Les titres sont séparés par des virgules
    var titre = $('input#titre').val().split(',').join('<br />');
    $('#ptitre').html(titre);

    // Change le texte de l'affectation
    var affectation = '';
    if (!(composanteIdCur.match(/^um3.*$/))) {
      affectation = composantes[cidx]['nom_long'];
    }
    if (affectation) affectation += '<br />';
    if ($('input#direction').val()) affectation += $('input#direction').val();
    if (affectation) affectation += '<br />';
    if ($('input#service').val()) affectation += $('input#service').val();
    $('#pservice').html(affectation);

    $('#padresse').html(adresses[aidx]['adresse_postale']);
    $('#pcomplement_adresse').html($('input#complement_adresse').val());

    // Affichage des numéros de téléphone
    // Affiche uniquement les numéros renseignés
    var numeros = {
      "tel": {
        "val": $('#tel').val(),
        "id": "ptel",
        "texte": "",
        "scheme": "tel"
      },
      "fax": {
        "val": $('#fax').val(),
        "id": "pfax",
        "texte": "Fax : ",
        "scheme": "fax"
      },
      "mobile": {
        "val": $('#mobile').val(),
        "id": "pmobile",
        "texte": "Mob : ",
        "scheme": "tel"
      }
    }
    var notempties = new Array();
    var spans = new Object();
    // Crée la liste des numeros renseignés
    var label;

    // Change le texte de l'établissement
    var $puniversite = $('<span id="puniversite">UNIVERSITÉ DE MONTPELLIER PAUL-VALÉRY</span><br />').css(styles);
    $('#petablissement').html($puniversite);


    for (var n in numeros) {
      var num = numeros[n];
      if (num['val'] != '') {
        notempties.push(n);
      }
      label = '<span id="' + num['id'] + 'label" style="color: ' + composantes[cidx]['couleur'] + ';">' + num['texte'] + '</span>';
      spans[n] = label + '<a href="' + num['scheme'] + ':' + num['val'] + '" title="' + num['scheme'] + ':' + num['val'] + '" style="color: #4A687C; text-decoration: none;"><span id="' + num['id'] + '" >' + num['val'] + '</span></a>';
    }
    var pslash = '<span id="pslash"> / </span>';
    $('#pnumeros1').remove();
    $('#pnumeros2').remove();
    var $pnumeros1 = $('<p id="pnumeros1" style="margin: 0; padding: 0;"></p>').css(styles);
    var $pnumeros2 = $('<p id="pnumeros2" style="margin: 0; padding: 0;"></p>').css(styles);
    if (notempties.length == 3) {
      $pnumeros1.html(spans[notempties[0]] + pslash + spans[notempties[1]]).insertAfter('#ptitre');
      $pnumeros2.html(spans[notempties[2]]).insertAfter('#pnumeros1');
    }
    if (notempties.length == 2) {
      $pnumeros1.html(spans[notempties[0]] + pslash + spans[notempties[1]]).insertAfter('#ptitre');
    }
    if (notempties.length == 1) {
      $pnumeros1.html(spans[notempties[0]]).insertAfter('#ptitre');
    }
    if (notempties.length == 0) {}

    $('#pnumeros1 a, #pnumeros2 a').css(styles);

    // Traitement du "par ordre"
    $('#pnomcomplet_po').remove();
    $('#pprenom_po').remove();
    $('#pnom_po').remove();
    var $pnomcomplet_po = $('<p id="pnomcomplet_po" style="margin: 0; padding: 0;"></p>');
    var $pprenom_po = $('<span id="pprenom_po"></span>').css(styles).css('color', composantes[cidx]['couleur']);
    var $pnom_po = $('<span id="pnom_po" style="text-transform: uppercase;"></span>').css(styles).css('color', composantes[cidx]['couleur']);
    if ($('#prenom_po').val() || $('#nom_po').val()) {
      $pprenom_po.html('P/O ' + $('#prenom_po').val() + ' ');
      $pnom_po.html($('#nom_po').val());
      $('#pnomcomplet').after($pnomcomplet_po);
      $pprenom_po.appendTo('#pnomcomplet_po');
      $pnom_po.appendTo('#pnomcomplet_po');
    }

    // Autres champs
    $('#pemail').html('<a href="mailto:' + $('input#email').val() + '" title="mailto:' + $('input#email').val() + '" style="text-decoration: none;">' + $('input#email').val() + '</a>');
    $('#pemail a').css(styles).css('color', '#009fe3');

    // Si le lien comporte http, on le laisse tel quel
    var lien = '';
    if (/https?:\/\//.test($('input#web').val())) {
      lien = $('input#web').val();
    } else {
      // Sinon ajoute http
      lien = 'http://' + $('input#web').val();
    }

    $('#pweb').html('<a href="' + lien + '" title="' + lien + '" style="text-decoration: none;">' + $('input#web').val() + '</a>');
    $('#pweb a').css(styles).css('color', '#009fe3');
    $('#logo-composante').attr('href', lien).attr('title', lien);
    $('#pinfos').html(nl2br($('#infos').val()));

    /*
     * Signature interne
     */

/**
    var si = '<pre>\n';
    si += $('#prenom').val() + ' ' + $('#nom').val().toUpperCase() + '\n';
    // Les titres sont séparés par des virgules
    titre = $('input#titre').val().split(',').join('\n');
    si += titre ? titre + '\n' : '';
    // Ajoute le nom de l'université
    si += 'Université de Montpellier Paul-Valéry'.toUpperCase() + '\n';
    // Ajoute éventuellement le nom de la composante
    if (!(composanteIdCur.match(/^um3.*$/))) {
      si += composantes[cidx]['nom_long'].toUpperCase() + '\n';
    }
    si += $('input#service').val() ? $('input#service').val() + '\n' : '';
    si += $('#pnumeros1').text() ? $('#pnumeros1').text() + '\n' : '';
    si += $('#pnumeros2').text()? $('#pnumeros2').text() + '\n' : '';
    si += '</pre>';
    $('#signature-interne').html(si);
  **/
    var si = '<pre>\n';
    si += $('#prenom').val() + ' ' + $('#nom').val().toUpperCase() + '\n';
    // Les titres sont séparés par des virgules
    titre = $('input#titre').val().split(',').join('\n');
    si += titre ? titre + '\n' : '';
    si += '<br/>';
    si += $('#pnumeros1').text() ? $('#pnumeros1').text() + '\n' : '';
    si += $('#pnumeros2').text()? $('#pnumeros2').text() + '\n' : '';
    si += $('input#direction').val() ? $('input#direction').val() + '\n' : '';
    si += $('input#service').val() ? $('input#service').val() + '\n' : '';
    si += $('#complement_adresse').val() ? $('#complement_adresse').val() + '\n' : '';
    si += $('input#email').val() ? $('input#email').val() + '\n' : '';
    si += '<br />';
    // Ajoute le nom de l'université
    si += 'Université de Montpellier Paul-Valéry'.toUpperCase() + '\n';
    // Ajoute éventuellement le nom de la composante
    if (!(composanteIdCur.match(/^um3.*$/))) {
      si += composantes[cidx]['nom_long'].toUpperCase() + '\n';
     }
    si += $('select#adresse_postale option:selected').text() ? $('select#adresse_postale option:selected').text() + '\n' : '';
    si += $('#web').val() ? $('#web').val() + '\n' : '';
    si += $('#infos').val() ? $('#infos').val() + '\n' : '';
   si += '</pre>';
    $('#signature-interne').html(si);
  
  }

  /*
   * Récupère les données en asynchrone, initialise puis prévisualise
   */

  $.getJSON("lib/get_json_data.php", function(data) {
    composantes = data["composantes"];
    adresses = data["adresses_postales"];
    initialise();
    previsu();
    // Rafraîchit toutes les 2 secondes
    setInterval(previsu, 2000);
  });
});
