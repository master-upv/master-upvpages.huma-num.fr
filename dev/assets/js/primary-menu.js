window.onload = function() {
    $("html").on('click', function(event) {
        if ($(event.target).closest('.hedone-menu-container, .sub-menu').length === 0) {
            $('ul.hedone-menu > li.menu-item-has-children > ul.sub-menu').removeClass('open');
            $('ul.hedone-menu > li.menu-item-has-children > a > i').removeClass('fa-caret-up');
        }
    });
    $('.nav-button').on('click', function(e) {
        e.preventDefault();
        $(this).toggleClass('open');
        $('.hedone-menu-container').toggleClass('open');
    });

    function initMainNavigation(container) {
        var screenReaderText = {
            expand: "expand child menu",
            collapse: "collapse child menu"
        };

        var dropdownToggle = $('<i />', {
            'class': 'fa fa-caret-down',
            'aria-expanded': false,
            'title': screenReaderText.expand
        }).append($('<span />', {
            'class': 'screen-reader-text',
            text: screenReaderText.expand
        }));

        container.find('ul.hedone-menu .menu-item-has-children > a').each(function() {
            if ($(this).find('i.fa-caret-down').length === 0) {
                $(this).append(dropdownToggle.clone());
            }
        });

        container.find('.menu-item-has-children').attr('aria-haspopup', 'true');

        container.find('.dropdown-toggle').on('click', function(e) {
            var _this = $(this),
                screenReaderSpan = _this.find('.screen-reader-text');

            e.preventDefault();
            _this.toggleClass('toggled-on');
            _this.next('.children, .sub-menu').toggleClass('toggled-on');

            _this.attr('aria-expanded', _this.attr('aria-expanded') === 'false' ? 'true' : 'false');
            screenReaderSpan.text(
                screenReaderSpan.text() === screenReaderText.expand ?
                screenReaderText.collapse :
                screenReaderText.expand
            );
        });

        $('ul.hedone-menu > li.menu-item-has-children > a').on('click', function(e) {
            var _this = $(this),
                activeSubMenu = _this.next('.sub-menu'),
                caret = _this.find('i.fa-caret-down');

            if (activeSubMenu.length) {
                e.preventDefault();
                $('ul.hedone-menu > li.menu-item-has-children > ul.sub-menu').not(activeSubMenu).removeClass('open');
                $('ul.hedone-menu > li.menu-item-has-children > a > i').not(caret).removeClass('fa-caret-up');

                caret.toggleClass('fa-caret-up');
                activeSubMenu.toggleClass('open');
            }
        });

        $('ul.hedone-menu ul.sub-menu li a i').on('click', function(e) {
            e.preventDefault();
            $(this).toggleClass('fa-caret-up');
            $(this).parent().next('.sub-menu').toggleClass('open');
        });
    }

    initMainNavigation($('#Hedone'));
};
//initialize pills
$(".nav-pills li a, .nav-pills li.active a").on('click', function(e) {
    e.preventDefault();
    $(this).tab('show');
});
//initialize popover
$('[data-toggle="popover"]').popover({
    placement: 'top',
    html: true,
    trigger: 'hover'
});
$('.html-false').popover({
    placement: 'top',
    html: false,
    trigger: 'hover'
});
