function loadSaveButton() {
    fetch('/assets/js/save-button.html')
        .then(response => response.text())
        .then(data => {
            document.querySelector('saveButton').innerHTML = data;
            // Dispatch a custom event indicating the footer is loaded
            document.dispatchEvent(new Event('saveButtonLoaded'));
        })
        .catch(error => console.error('Erreur de chargement du saveButton:', error));
}
loadSaveButton();

function expandButton() {
    var button = document.querySelector('.save-button');
    button.classList.toggle('expanded');
}

function printPage() {
    document.execCommand('print');
}

function emailPage() {
    var subject = encodeURIComponent(document.title);
    var body = encodeURIComponent("Voici un lien vers la page : " + window.location.href);
    window.location.href = "mailto:?subject=" + subject + "&body=" + body;
}

function addToFavorites() {
    var title = document.title;
    var url = window.location.href;

    if (window.external && ('AddFavorite' in window.external)) { // Pour Internet Explorer
        window.external.AddFavorite(url, title);
    } else if (window.sidebar && window.sidebar.addPanel) { // Pour Firefox <=22
        window.sidebar.addPanel(title, url, '');
    } else if (window.opera && window.print) { // Pour Opera <=12
        this.title = title;
        return true;
    } else { // Pour les autres navigateurs modernes
        alert('Appuyez sur Ctrl+D (ou Cmd+D pour Mac) pour ajouter cette page à vos favoris.');
    }
}