// conseils_data.js
const conseilsData = {
    categories: {
        attitude: {
            title: "Attitude et Perception",
            globalAdvice: "Votre attitude générale est analysée en fonction de vos réponses.",
            criteria: [
                {
                    question: "Score d'attitude",
                    thresholds: [1, 40, 80],
                    advice: {
                        high: "Vous avez une attitude **très positive** envers l'accessibilité numérique. Continuez ainsi et partagez votre expérience avec vos collègues.",
                        medium: "Vous semblez engagé(e) pour l'accessibilité et la pédagogie inclusive, mais peut-être que vous redoutez des difficultés techniques ou de temps. Voyons si le reste de ce rapport peut vous aider.",
                        low: "Peut-être que l'accessibilité et la pédagogie inclusive vous intéressent mais ne sont pas votre priorité, ou que vous êtes pessimiste quant à l'application concrète de ces concepts. Espérons que ce rapport vous aide à y voir plus clair."
                    }
                },
                {
                    question: "Comment évalueriez-vous votre connaissance actuelle des besoins des étudiants en situation de handicap ?",
                    thresholds: [1, 4, 6],
                    advice: {
                        high: "Vous connaissez bien les besoins des étudiants en situation de handicap, vous pourriez être une personne ressource au sein de l'équipe pédagogique à laquelle vous appartenez.",
                        medium: "Vous déclarez avoir une connaissance moyenne des besoins des étudiants en situation de handicap. Nous vous suggérons de consulter les vidéos de (Handi-formation)[https://pod.univ-montp3.fr/handi-upvm3/], elles sont souvent appuyées par des témoignages ou des cas d'études.",
                        low: "Vous avez déclaré ne pas vraiment connaître les besoins des étudiants en situation de handicap. En plus des vidéos de (Handi-formation)[https://pod.univ-montp3.fr/handi-upvm3/], nous vous conseillons de tester l'extension de navigateur (Funkify)[https://www.funkify.org/simulators/], qui permet de simuler des troubles sensoriels, cognitif et psychomoteurs."
                    }
                },
                {
                    question: "Quelle est votre disposition à investir du temps et des efforts pour rendre vos cours plus accessibles ?",
                    thresholds: [1, 4, 6],
                    advice: {
                        high: "Vous avez déclaré avoir du temps à consacrer à cette mission pour rendre vos cours plus accessible, **merci** pour vos étudiants. Peut-être que c'est une thématique qui vous tient à coeur, auquel cas, nous vous encourageons à suivre des formations comme celles présentées sur [FUN-MOOC](https://www.fun-mooc.fr/fr/actualites/accessibilite-numerique-vers-des-formations-toujours-plus-inclus/) ou [design.numerique.gouv.fr](https://design.numerique.gouv.fr/formations/accessibilite/). Peut-être d'ailleurs que si l'accessibilité est déjà acquise, vous pourriez vous intéresser à [la pédagogie inclusive](https://www.enseigner.ulaval.ca/sites/default/files/guide_app_inclusive.pdf) et [la conception universelle de l'apprentissage](https://innovation.sainteanne.ca/wp-content/uploads/2018/03/Conception_universelle_apprentissage.pdf).",
                        medium: "Vous avez un peu de temps à consacrer à rendre vos cours accessibles. Notre conseil est de proposer à tout support interactif une transcription sur un PDF accessible, et pour vous y aider, voici une [excellente ressource](https://www.avh.asso.fr/fr/favoriser-laccessibilite/accessibilite-numerique/accessibilite-des-documents-pdf). En complément, nous vous encourageons à organiser des visio ou des forums avec vos étudiants pour clarifier les incertitudes dans votre cours, il s'agit de la meilleure manière de lever des ambiguïtés liées à des problèmes cognitifs par exemple.",
                        low: "Vous ne pouvez pas consacrer trop de temps à améliorer l'accessibilité de votre cours, peut-être que vous pourriez commencer par [vérifier l'accessibilité de vos cours](https://graphiste.com/blog/outils-test-accessibilite-site-internet/), et vous concentrer sur les supports problématiques. Aussi, ouvrez un forum pour permettre aux étudiants de signaler des difficultés. Repérer les problèmes vous permet d'y palier rapidement, avec l'aide d'un ingénieur pédagogique si besoin."
                    }
                },
                // Ajoutez d'autres critères de questions pour cette catégorie
            ]
        },
        normes: {
            title: "Normes d'accessibilité",
            globalAdvice: "Vos réponses en matière de normes d'accessibilité ont été analysées.",
            criteria: [
                {
                    question: "Score Accessibilité",
                    thresholds: [1, 40, 80],
                    advice: {
                        high: "Vous connaissez bien les normes d'accessibilité et être conscient(e) des enjeux réglementaires.",
                        medium: "Vous ne maîtrisez pas les normes d'accessibilité mais vous semblez conscient(e) des enjeux réglementaires.",
                        low: "Il est probable que vous découvriez les normes d'accessibilité et leurs enjeux réglementaires."
                    }
                },
                {
                    question: "Connaissez-vous les exigences légales en matière d'accessibilité numérique pour les sites web et les ressources pédagogiques ?",
                    thresholds: [1, 3, 6],
                    advice: {
                        high: "Vous êtes sensibilisé aux exigences réglementaires, que dites-vous d'explorer l'application de ces lois ? Voici un excellent dossier de la [Fédération des aveugles de France](https://aveuglesdefrance.org/app/uploads/2022/05/etude-conformite-1400-19052022.pdf). Ils ont quelques chiffres intéressants pour motiver les collègues à la prochaine réunion.",
                        medium: "Vous savez qu'il y a des exigences légales, mais peut-être que vous n'en mesurez par la portée. Le [schéma directeur handicap](https://www.univ-montp3.fr/fr/file/84726/) de notre université expose très bien ces exigences de la page 6 à 9.",
                        low: "Vous avez cerné que l'égalité des chances à l'université passait probablement par l'accessibilité et les services d'accompagnement au handicap, mais peut-être que vous ne connaissez pas les obligations légales en matière d'accessibilité numérique. Le [schéma directeur handicap](https://www.univ-montp3.fr/fr/file/84726/) de notre université expose très bien ces exigences de la page 6 à 9."
                    }
                },
                // Ajoutez d'autres critères de questions pour cette catégorie
            ]
        }
        // Ajoutez d'autres catégories selon vos besoins
    }
};

export default conseilsData;
