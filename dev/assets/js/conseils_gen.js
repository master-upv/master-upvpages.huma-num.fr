// conseils_gen.js
import conseilsData from '/assets/js/conseils_data.js';
import { marked } from "https://cdn.jsdelivr.net/npm/marked/lib/marked.esm.js";

function generateAdvices(questionnaireData) {
    const accordion = document.getElementById('accordionConseils');
    accordion.innerHTML = ''; // Clear existing content

    Object.keys(conseilsData.categories).forEach(categoryKey => {
        const category = conseilsData.categories[categoryKey];
        let globalScore = 0;
        let criteriaCount = 0;

        // Crée un élément pour chaque catégorie
        const categoryPanelButton = document.createElement('div');
        categoryPanelButton.className = 'panel btn btn-block btn-default collapsed';
        categoryPanelButton.setAttribute('data-toggle', 'collapse');
        categoryPanelButton.setAttribute('data-target', `#collapse${categoryKey}`);
        categoryPanelButton.dataset.parent = '#accordionConseils';
        categoryPanelButton.setAttribute('aria-expanded', 'false');
        categoryPanelButton.setAttribute('role', 'button');
        categoryPanelButton.innerHTML = category.title;

        const categoryContent = document.createElement('div');
        categoryContent.id = `collapse${categoryKey}`;
        categoryContent.className = 'panel-collapse white-background collapse';
        categoryContent.setAttribute('aria-expanded', 'false');
        categoryContent.style.height = '0px';

        const panelBody = document.createElement('div');
        panelBody.className = 'panel-body';
        const globalAdviceParagraph = document.createElement('p');
        globalAdviceParagraph.innerHTML = marked(category.globalAdvice);
        panelBody.appendChild(globalAdviceParagraph);

        const criteriaList = document.createElement('ul');

        category.criteria.forEach(criteria => {
            const questionResponse = questionnaireData.body.fields.find(field => field.name === criteria.question);
            if (questionResponse) {
                criteriaCount++;
                let adviceLevel = 'low';
                if (questionResponse.value >= criteria.thresholds[2]) {
                    adviceLevel = 'high';
                } else if (questionResponse.value >= criteria.thresholds[1]) {
                    adviceLevel = 'medium';
                }

                globalScore += questionResponse.value;

                const adviceItem = document.createElement('li');
                adviceItem.innerHTML = marked(criteria.advice[adviceLevel]);
                criteriaList.appendChild(adviceItem);
            }
        });

        panelBody.appendChild(criteriaList);
        categoryContent.appendChild(panelBody);
        accordion.appendChild(categoryPanelButton);
        accordion.appendChild(categoryContent);
    });
}

// Charger les données lors du chargement de la page
document.addEventListener('DOMContentLoaded', function() {
    generateAdvices(questionnaireData);
});
