function loadFooter() {
    fetch('/assets/footer.html')
        .then(response => response.text())
        .then(data => {
            document.querySelector('footer').innerHTML = data;
            // Dispatch a custom event indicating the footer is loaded
            document.dispatchEvent(new Event('footerLoaded'));
            
        })
        .catch(error => console.error('Erreur de chargement du footer:', error));
}
loadFooter();