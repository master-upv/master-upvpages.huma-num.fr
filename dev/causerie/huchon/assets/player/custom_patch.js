 // Fonction pour remplacer les canvas par des iframes
 function replaceCanvasWithIframe(videoData) {
    for (let videoInfo of videoData) {
        var canvas = document.getElementById(videoInfo.canvasId); // ID du canvas où la vidéo devrait apparaître
        
        if (canvas) {
            // Créer une iframe pour remplacer le canvas
            var iframe = document.createElement('iframe');
            iframe.src = videoInfo.videoUrl;  // URL de la vidéo (YouTube, Vimeo, etc.)
            iframe.width = canvas.width;
            iframe.height = canvas.height;
            iframe.setAttribute('frameborder', '0');
            iframe.setAttribute('allow', 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture');
            iframe.setAttribute('allowfullscreen', true);

            // Remplacer le canvas par l'iframe
            canvas.parentNode.style.pointerEvents = 'all';
            canvas.parentNode.replaceChild(iframe, canvas);
        }
    }
}
 function replaceCanvasWithGif(gifData) {
    for (let gifInfo of gifData) {
        var canvas = document.getElementById(gifInfo.canvasId); // ID du canvas où la vidéo devrait apparaître
        
        if (canvas) {
            // Créer une iframe pour remplacer le canvas
            var img = document.createElement('img');
            img.src = gifInfo.gifUrl;  // URL de du gif
            img.width = canvas.width;
            img.height = canvas.height;

            // Remplacer le canvas par l'iframe
            canvas.parentNode.replaceChild(img, canvas);
        }
    }
}

// Exemple de données vidéo associées à des canvas
var videoData = [
        { canvasId: '9E279EE3258117948FF1C73880A9A7D9', videoUrl: 'https://www.youtube.com/embed/OgEed_j5JCM' }, //Parcours
        { canvasId: '6ACD0386BBC9BDBE37AD47898BA27C35', videoUrl: 'https://www.youtube.com/embed/yIK989L1J8c' }, //Horizons
        { canvasId: 'CDF6ADBE953B5BE3634047CC7FA727BF', videoUrl: 'https://www.youtube.com/embed/z8F3joaSxn4' }, //AntiFakeNewsAI
        { canvasId: '9C00827A35452E61A6F83C70ADD79B4E', videoUrl: 'https://www.youtube.com/embed/w7NsxiMiq6E' }, //Paradigme
        { canvasId: 'A26B3C3E9962C3A7BB7125B51BCC8D9D', videoUrl: 'https://www.youtube.com/embed/1pi3D1-5K_I' }, //Influence
        { canvasId: '7A16D933861AD40ECBF5FE2F8742649F', videoUrl: 'https://www.youtube.com/embed/pMsGoO2YU_Q' }, //Inspiration
        { canvasId: '508947BCBA09FD4145CDE43C425BFB32', videoUrl: 'https://www.youtube.com/embed/xKI9XEiAI4o' }, //Comment
        { canvasId: 'AD2422E1313421C84FBB84E81339534C', videoUrl: 'https://www.youtube.com/embed/uR_vA_ms41s' }, //Pourquoi
        { canvasId: 'ABE82D93BFE900114A992A8CC236A5D7', videoUrl: 'https://www.youtube.com/embed/SjsJGw5GQcc' }, //Jamais
        //{ canvasId: 'XXXXX', videoUrl: 'https://www.youtube.com/embed/wLYO7Fadrh4' }, Communauté
        { canvasId: 'XXXXX', videoUrl: 'https://www.youtube.com/embed/RytkwMtfd4Q' }, //Biblio 
        { canvasId: 'B1D124E227378786B5D86C958538169A', videoUrl: 'https://www.youtube.com/embed/_fDbSfMOL3I' }, //Veille
    ];
// Gif à remplacer
var gifData = [
        {canvasId: 'FBD5B30A86D0F6331DDE110888D04AE9', gifUrl: 'assets/player/sound_dots.gif'},
];
// Fonction qui vérifie régulièrement si les canvas sont présents
function checkAndReplaceCanvas() {
    let interval = setInterval(() => {
        let allCanvasReplaced = true;
        
        videoData.forEach(videoInfo => {
            var canvas = document.getElementById(videoInfo.canvasId);

            if (canvas) {
                console.log('Canvas détecté :', videoInfo.canvasId);
                replaceCanvasWithIframe([videoInfo]); // Remplace uniquement le canvas concerné
            } else {
                allCanvasReplaced = false;
            }
        });
        gifData.forEach(gifInfo => {
            var canvas = document.getElementById(gifInfo.canvasId);

            if (canvas) {
                console.log('Canvas détecté :', gifInfo.canvasId);
                replaceCanvasWithGif([gifInfo]); // Remplace uniquement le canvas concerné
            } else {
                allCanvasReplaced = false;
            }
        });

        // Si tous les canvas ont été remplacés, on arrête le setInterval
        if (allCanvasReplaced) {
            clearInterval(interval);
            console.log('Tous les canvas ont été remplacés par des iframes.');
        }
    }, 1000); // Vérification toutes les secondes
}

// Déclencher la vérification une fois la page complètement chargée
window.addEventListener("pageshow", (event) => {
    checkAndReplaceCanvas();
    var home_button = document.getElementById('C837AA12AABA60A465A908FA984FA2EE');
    home_button.parentNode.style.pointerEvents = 'all';
});
